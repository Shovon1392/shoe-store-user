import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends BaseComponent implements OnInit {

  ngOnInit(): void {
  }

  /** Go to next page */
  goNext(page){
    this.router.navigateByUrl(page);
  }
  /** End of class */

}
