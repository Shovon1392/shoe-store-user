import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { AccountComponent } from './components/account/account.component';
import { AuthComponent } from './components/auth/auth.component';
import { ProductDetailComponent } from './components/product-detail/product-detail.component';
import { StoreComponent } from './components/store/store.component';
import { WishlistComponent } from './components/wishlist/wishlist.component';
import { DefaultComponent } from './layouts/default/default.component';
// import { FullwidthComponent } from './layouts/fullwidth/fullwidth.component';

const routes: Routes = [
  { path: '', redirectTo: 'stores', pathMatch:'full'},
  {
    path : '',
    component : DefaultComponent,
    children : [{
      path : 'stores',
      component : HomeComponent
     }
     , {
       path : 'stores/:storeId',
       component : StoreComponent
     },
     {
      path : 'stores/:storeId/:productId/:colorId/:sizeId',
      component : ProductDetailComponent
     },
     {
      path : 'wishlist',
      component : WishlistComponent
     },
     {
      path:'authentication',
      component : AuthComponent,
      },
     {
       path : 'account',
       component : AccountComponent
     }
    ]
   }
   /* {
    path : '',
    component : FullwidthComponent,
    children : [
      {
      path:'login',
      component : AuthComponent,
      }
   ]
  }, */
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
