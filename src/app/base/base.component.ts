import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { from } from 'rxjs';
import {ProductService } from '../Services/productService/product.service';
import { SellerService } from 'src/app/services/sellerService/seller.service';
import { UserService } from 'src/app/services/userService/user.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit {

  constructor(
    public activateRoute : ActivatedRoute,
    public router: Router,
    public spinner: NgxSpinnerService,
    public location : Location,
    /**Custom services */
    public productService: ProductService,
    public sellerService : SellerService,
    public userService : UserService,
  ) { }
  noImage = 'https://thumbs.dreamstime.com/b/no-image-available-icon-flat-vector-no-image-available-icon-flat-vector-illustration-132482953.jpg';

  ngOnInit(): void {
  }

  /** variables */
  selectedShop = JSON.parse(localStorage.getItem('lastShop'));
  userDetails = JSON.parse(localStorage.getItem('userDetails'));
  /**
   * Custom log
   */
  log(data){
    console.log(data);
  }

  /** get random images */
getRandomImages(images){

  //console.log(images)
  if(images.length > 0 ){
    // setInterval(function(){ 
      let randomNo = Math.floor(Math.random() * (images.length)); 
      console.log(randomNo)
      return images[randomNo]['thumb_image'];
    // }, 10000);
  } else {
    return this.noImage;
  }
}


/** GO to product details page */

getProductDetails(shopId,productId){
  this.router.navigateByUrl('stores/'+shopId+'/'+productId);
}
/**
   * End of classs
   */
}
