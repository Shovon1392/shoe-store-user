import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { timeout, catchError } from 'rxjs/operators';
import {RootService} from '../root.service';
import { IProductDetails } from 'src/app/interface/Product/iProduct';

@Injectable({
  providedIn: 'root'
})
export class ProductService  extends RootService{

  constructor(http: HttpClient){
    super(http);
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
       //'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers
    };
  }

 /** Get product details by color size */
getProductDetailsByColorSize(data : IProductDetails){
  console.log(this.product_details, data )
  return this.http.post(this.product_details,data,this.httpOptions).pipe(
    timeout(this.timeOutLimit)
  );
}

}
