import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { RouterModule } from '@angular/router';


import { DefaultComponent} from './default.component';
import { SharedModule } from '../../shared/shared.module';
import { StoreComponent } from 'src/app/components/store/store.component';
import { HomeComponent } from 'src/app/components/home/home.component';
import { ProductDetailComponent } from 'src/app/components/product-detail/product-detail.component';
import { AuthComponent } from 'src/app/components/auth/auth.component';
import { WishlistComponent } from 'src/app/components/wishlist/wishlist.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    DefaultComponent,
    HomeComponent,
    StoreComponent,
    ProductDetailComponent,
    AuthComponent,
    WishlistComponent

  ],
  imports: [
    CommonModule,
    NgxSpinnerModule,
    SharedModule,
    RouterModule,
    FormsModule
  ]
})
export class DefaultModule { }
