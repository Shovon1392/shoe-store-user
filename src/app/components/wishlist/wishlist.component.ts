import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.scss']
})
export class WishlistComponent extends BaseComponent implements OnInit {


  wishlists : any = [];
  total_mrp=0;
  total_saleprice=0;
  total_discount=0;
  ngOnInit(): void {
    this.getUserWishList(this.userDetails.userId)
  }


  getUserWishList(userId){
    this.wishlists = [];
    this.total_saleprice = 0;
    if(userId){
      try {
        this.spinner.show()
        this.userService.getUserWishlists(userId).subscribe(res => {
          this.spinner.hide();
          if (res['data'][0].length > 0) {
            res['data'][0].forEach(element => {
              let tempdata = (element);

              tempdata.images = (tempdata.images) ? JSON.parse("[" + JSON.parse(tempdata.images) + "]") : [];
              this.wishlists.push(tempdata);
              this.total_saleprice += tempdata.selling_price;
              this.total_mrp += tempdata.mrp
            })
            this.total_discount = this.total_mrp - this.total_saleprice;
            
          } else  {
          alert('Product added to your wishlist')
         }
        }, err => {
          this.spinner.hide();
          console.log(err)
        })
      } catch (error) {
        this.spinner.hide();
      }
    } else {
      alert('Login to see wishlist')
    }
  }


  /** GO to product details  */

  /**End of class */
}
