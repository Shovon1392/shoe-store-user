import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends BaseComponent implements OnInit {

  storeList : any = [];
  selectedLocation = 'durgapur';
  ngOnInit(): void {
    this.getStores(this.selectedLocation);
  }


  /** Get shop list by location */
  getStores(location){
    console.log(location)
    this.spinner.show();
    this.sellerService.getSellerByLocation(location).subscribe(
      res =>{
        this.spinner.hide();
        this.storeList = res['data'][0];
        console.log(res);
      }, err =>{
        this.spinner.hide();
        console.log(err);
      }
    );
  }

  /** go to product list page*/
  getProductList(store){
    localStorage.setItem('lastShop', JSON.stringify(store));
    this.router.navigateByUrl('stores/'+store.shopId)
  }
  /** End of class */
}
 