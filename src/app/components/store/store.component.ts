import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent  extends BaseComponent implements OnInit {



  shopProduct: any = [];
  shopId : any;
  ngOnInit(): void {
    this.activateRoute.params.subscribe(params => {
      console.log(params)
      this.getAllshopProduct(params['storeId']);
      this.shopId = params['storeId'];
      // Print the parameter to the console.
  });

  }

 /*  this.productDetails.all_colors = ( this.productDetails.all_colors) ? JSON.parse("[" + JSON.parse( this.productDetails.all_colors)[0] + "]") : [];
        this.productDetails.available_colors = ( this.productDetails.available_colors) ? JSON.parse("[" + JSON.parse( this.productDetails.available_colors)[0] + "]") : [];
        this.productDetails.available_sizes = ( this.productDetails.available_sizes) ? JSON.parse("[" + JSON.parse( this.productDetails.available_sizes)[0] + "]") : [];
        this.productDetails.all_sizes = ( this.productDetails.all_sizes) ? JSON.parse("[" + JSON.parse( this.productDetails.all_sizes)[0] + "]") : [];
        this.productDetails.images = ( this.productDetails.images) ? JSON.parse("[" + JSON.parse( this.productDetails.images)[0] + "]") : [];
 */

  /**
   * GEt all products
   */
    getAllshopProduct(shopId) {
      try {
        this.shopProduct = [];
        this.spinner.show();
        this.sellerService.getShopPrroductList(shopId).subscribe(res => {
          this.spinner.hide();
          if (res['data'][0].length > 0) {
            res['data'][0].forEach(element => {
              let tempdata = (element);
              tempdata.colors = (tempdata.colors) ? JSON.parse("[" + JSON.parse(tempdata.colors)[0] + "]") : [];
              tempdata.sizes = (tempdata.sizes) ? JSON.parse("[" + JSON.parse(tempdata.sizes) + "]") : [];
              tempdata.images = (tempdata.images) ? JSON.parse("[" + JSON.parse(tempdata.images) + "]") : [];
              tempdata.products = (tempdata.products) ? JSON.parse("[" + JSON.parse(tempdata.products) + "]") : [];
              if(tempdata.colors.length > 0 && tempdata.sizes.length > 0){
                this.shopProduct.push(tempdata);
              }
            });
            console.log(this.shopProduct)
          }
        }, err => {
          this.spinner.hide();
          console.log(err)
        })
      } catch (error) {
        this.spinner.hide();
      }
    }

  /** Get product details of initial shop product  */

  getProductDetails(shopProduct){
    console.log(shopProduct)
    this.router.navigateByUrl('stores/'+this.shopId+'/'+shopProduct['master_product']+'/'+shopProduct['colors'][0]['colorId']+'/'+shopProduct['sizes'][0]['sizeId']);
  }

  /** End of class */
  }
