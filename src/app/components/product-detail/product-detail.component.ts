import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';
import { IProductDetails } from 'src/app/interface/Product/iProduct';
import { IAddWishlist } from 'src/app/interface/User/iuser';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent extends BaseComponent implements OnInit {

  productDetails: any;
  shopId : any;
  addDataToWishlist : IAddWishlist = {
    user_id : (this.userDetails) ? this.userDetails.userId : null,
    product_id : ''
  };

  getProductDetailsData: IProductDetails ={
    product_color : '',
    product_size: '',
    shop_id : '',
    master_product : ''
  }

  slideIndex = 1;

  ngOnInit(): void {
    this.activateRoute.params.subscribe(params => {
      console.log(params)
      this.getProductDetailsData ={
        product_color : params['colorId'],
        product_size: params['sizeId'],
        shop_id : params['storeId'],
        master_product : params['productId']
      }
      this.getProductDetails(this.getProductDetailsData);
      this.addDataToWishlist.product_id = params['productId'];
      this.shopId = params['storeId'];
      // Print the parameter to the console.
  });
}

getProductDetails( data : IProductDetails ) {
  try {
    this.productDetails = [];
    this.spinner.show()
    this.productService.getProductDetailsByColorSize(data).subscribe(res => {
      this.spinner.hide();
      this.productDetails = res['data'][0][0];

       console.log(res);
      // console.log(this.productDetails,JSON.parse("[" + JSON.parse( this.productDetails.all_colors)[0] + "]"));

      // console.log(JSON.parse( this.productDetails.all_colors));
      this.productDetails.all_colors = ( this.productDetails.all_colors) ? JSON.parse("[" + JSON.parse( this.productDetails.all_colors)[0] + "]") : [];
      this.productDetails.available_colors = ( this.productDetails.available_colors) ? JSON.parse("[" + JSON.parse( this.productDetails.available_colors)[0] + "]") : [];
      this.productDetails.available_sizes = ( this.productDetails.available_sizes) ? JSON.parse("[" + JSON.parse( this.productDetails.available_sizes)[0] + "]") : [];
      this.productDetails.all_sizes = ( this.productDetails.all_sizes) ? JSON.parse("[" + JSON.parse( this.productDetails.all_sizes)[0] + "]") : [];
      this.productDetails.images = ( this.productDetails.images) ? JSON.parse("[" + JSON.parse( this.productDetails.images)[0] + "]") : [];



      if(this.productDetails.images.length > 0){
        this.showImage(this.productDetails.images[0]['image'])
      }else{
        this.showImage(this.noImage)
      }
    }, err => {
      this.spinner.hide();
      console.log(err)
    })
  } catch (error) {
    this.spinner.hide();
  }
}




  showImage(imgs) {
    var expandImg = document.getElementById("expandedImg");
    var imgText = document.getElementById("imgtext");
    expandImg['src'] = imgs;
    expandImg.parentElement.style.display = "block";
  }

  scrollThumbnail(direction)
  {

     if(direction == 0)
     {
       document.getElementById("scrollDiv").scrollLeft -= 200;
     }
     else{
      document.getElementById("scrollDiv").scrollLeft += 200;
     }
  }


  /** Add to wishlist */
  addToWishlist(){
    if(this.addDataToWishlist.product_id && this.addDataToWishlist.user_id ){
      try {
        this.spinner.show()
        this.userService.addProductToWishlist(this.addDataToWishlist).subscribe(res => {
          this.spinner.hide();
         if(!res['data']['affectedRows']){
           alert('Product already added to your wishlist')
         } else if(res['data']['affectedRows']) {
          alert('Product added to your wishlist')
         }
        }, err => {
          this.spinner.hide();
          console.log(err)
        })
      } catch (error) {
        this.spinner.hide();
      }
    } else {
      alert('Please select product and user')
    }
  }
  /** End of class */
}
