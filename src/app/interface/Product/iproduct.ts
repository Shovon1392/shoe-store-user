/** For getting product details  */

export interface IProductDetails{
  product_color: string,
  product_size: string,
  shop_id : string,
  master_product : string
}
